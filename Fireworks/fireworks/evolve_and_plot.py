import numpy as np
from fireworks import ic
from fireworks.ic import *
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from IPython.display import HTML, display

def evolve_system_particles(particles, acceleration_dynamics, integrator, total_time, initial_timestep, adaptive_timestep_func=None, fast_sim = False):
    """
    Evolves a system of particles over time using the specified integrator.

    Args:
    - particles: Initial particle configuration.
    - acceleration_dynamics: Function defining acceleration dynamics.
    - integrator: Function implementing the integration scheme.
    - total_time: Total simulation time.
    - initial_timestep: Initial time step for integration.
    - adaptive_timestep_func: Function for adaptive time step calculation.

    Returns:
    - particle_positions: List of particle positions over time.
    - times: List of corresponding time values.
    """
    particle_positions = []
    times = []
    
    t = 0
    timestep = initial_timestep

    while t < total_time:
        particles, _, _, _, _ = integrator(particles, timestep, acceleration_dynamics, 1e-6)
        particle_positions.append(particles.pos.copy())
        
        # Update time and check for adaptive time step
        if adaptive_timestep_func:
            timestep = adaptive_timestep_func(particles, 1e-7, 1e-5)
            t += timestep

        else:
            if fast_sim == True:
                if timestep == 0.0001:
                    t += timestep * 10
                if timestep == 0.001:
                    t += timestep * 1
                if timestep == 0.01:
                    t += timestep * 10
                else:
                    t += timestep
            else:
                t += timestep
            
        times.append(t)

    return particle_positions, times

def evolve_system_energies(particles, acceleration_dynamics, integrator, total_time, initial_timestep, adaptive_timestep_func=None, fast_sim = False):
    """
    Evolves a system of particles and computes energy over time.

    Args:
    - particles: Initial particle configuration.
    - acceleration_dynamics: Function defining acceleration dynamics.
    - integrator: Function implementing the integration scheme.
    - total_time: Total simulation time.
    - initial_timestep: Initial time step for integration.
    - adaptive_timestep_func: Function for adaptive time step calculation.

    Returns:
    - times: List of time values.
    - energies: List of corresponding energy values.
    - energy_errors: List of relative energy errors.
    """
    initial_energy, _, _ = particles.Etot()
    energies = []
    times = []

    t = 0
    timestep = initial_timestep

    while t < total_time:
        particles, _, _, _, _ = integrator(particles, timestep, acceleration_dynamics)
        current_energy, _, _ = particles.Etot()
        energies.append(current_energy)
        
        # Update time and check for adaptive time step
        if adaptive_timestep_func:
            timestep = adaptive_timestep_func(particles, 1e-7, 1e-4)
            t += timestep

        else:
            if fast_sim == True:
                if timestep == 0.0001:
                    t += timestep * 10
                if timestep == 0.001:
                    t += timestep * 1
                if timestep == 0.01:
                    t += timestep * 1
                else:
                    t += timestep
            else:
                t += timestep
            
        times.append(t)

    energy_errors = np.abs((energies - initial_energy) / initial_energy)

    return times, energies, energy_errors

def evolve_and_plot_particles(particle_list, integrator_name, num_particles, ax=None):
    """
    Plots the path of particles for a given integrator.

    Args:
    - particle_list: List of particle positions over time.
    - integrator_name: Name of the integrator used.
    - ax: Axes object for plotting.

    If ax is provided, it uses it for plotting; otherwise, creates a new subplot.
    """
    if ax is None:
        _, ax = plt.subplots(figsize=(15, 7))

    particle_array = np.array(particle_list)
    ax.plot(particle_array[:, :, 0][:, 0], particle_array[:, :, 1][:, 0], label='Particle 1')
    ax.plot(particle_array[:, :, 0][:, 1], particle_array[:, :, 1][:, 1], label='Particle 2')
    if num_particles == 3:
        ax.plot(particle_array[:, :, 0][:, 2], particle_array[:, :, 1][:, 2], label='Particle 3')
    else:
        pass
    ax.set_title(f'{integrator_name} Path of Particles', fontsize=10)
    ax.set_xlabel('X-axis')
    ax.set_ylabel('Y-axis')
    ax.legend()

    if ax is None:
        plt.tight_layout()
        plt.show()

def evolve_and_plot_energies(times, E_err, timesteps, integrator_name, num_integrators, adaptive_timestep_func=None, E_or_E_err="Energy Error"):
    """
    Plots energy or energy error over time for multiple integrators.

    Args:
    - times: List of time values.
    - E_err: List of energy errors.
    - timesteps: List of corresponding time steps.
    - integrator_name: List of integrator names.
    - num_integrators: Number of integrators used.
    - E_or_E_err: Specifies whether to plot energy or energy error.
    """
    _, axes = plt.subplots(1, num_integrators, figsize=(15, 5))

    n_ts = len(timesteps)
    for i in range(0, num_integrators):
        for j, ts in enumerate(timesteps):
            if E_or_E_err == "Energy Error":
                if adaptive_timestep_func:
                    axes[i].plot(times[i * n_ts + j], E_err[i * n_ts + j], label=f'Initial ts = {ts}')
                    axes[i].set_title(f'{integrator_name[i * n_ts]} {E_or_E_err}')
                else:
                    axes[i].plot(times[i * n_ts + j], E_err[i * n_ts + j], label=f'Timestep = {ts}')
                    axes[i].set_title(f'{integrator_name[i * n_ts]} {E_or_E_err}')
                axes[i].set_xlabel('time [steps]')
                axes[i].set_ylabel('|E_t - E_i/ E_i|')
                axes[i].set_yscale('log')
                axes[i].legend()
            else:
                if adaptive_timestep_func:
                    axes[i].plot(times[i * n_ts + j], E_err[i * n_ts + j], label=f'Initial ts = {ts}')
                    axes[i].set_title(f'{integrator_name[i * n_ts]} {E_or_E_err}')
                else:
                    axes[i].plot(times[i * n_ts + j], E_err[i * n_ts + j], label=f'Timestep = {ts}')
                    axes[i].set_title(f'{integrator_name[i * n_ts]} {E_or_E_err}')
                axes[i].set_xlabel('time [steps]')
                axes[i].set_ylabel('Energy')
                axes[i].legend()

    plt.tight_layout()
    plt.show()

def animate_particle_path_3D(times_list, particle_list, integrator_name, animation_name, _):
    """
    Animates the 3D path of particles over time.

    Parameters:
    - times_list (list): List of time values corresponding to each frame.
    - particle_list (list): List of particle positions for each frame.
    - integrator_name (str): Name of the integrator used.
    - ecc (float): Eccentricity value.

    Returns:
    - Animation: Displays the 3D animation.
    """
    max_x = max(particle[0] for frame in particle_list for particle in frame)
    min_x = min(particle[0] for frame in particle_list for particle in frame)
    max_y = max(particle[1] for frame in particle_list for particle in frame)
    min_y = min(particle[1] for frame in particle_list for particle in frame)
    max_z = max(particle[2] for frame in particle_list for particle in frame)
    min_z = min(particle[2] for frame in particle_list for particle in frame)

    def update(frame):
        ax.clear()
        ax.set_title(f'Time: {times_list[frame]} for {integrator_name}', color='white')

        for i, particles in enumerate(particle_list[frame]):
            x, y, z = particles
            size = 200 if i == 0 else 100
            color = 'blue' if i == 0 else 'orange'
            ax.scatter(x, y, z, marker='o', s=size, color=color, label='Particle')

            if frame == 0:
                particle_paths[i] = np.array([[x, y, z]])
            else:
                particle_paths[i] = np.concatenate([particle_paths[i], [[x, y, z]]])

            ax.plot3D(
                particle_paths[i][:, 0], 
                particle_paths[i][:, 1], 
                particle_paths[i][:, 2],
                color=color,  # Use the assigned color here
                linestyle='--',
                alpha=0.5
            )

        ax.set_xlabel('X', color='white')
        ax.set_ylabel('Y', color='white')
        ax.set_zlabel('Z', color='white')
        ax.legend()

        ax.set_xlim(min_x - 1, max_x + 1)
        ax.set_ylim(min_y - 1, max_y + 1)
        ax.set_zlim(min_z - 1, max_z + 1)

        ax.xaxis.pane.fill = ax.yaxis.pane.fill = ax.zaxis.pane.fill = 'black'
        ax.grid(True, linestyle='-', linewidth=0.5, alpha=0.5, color='black')  # Set grid color to black

    fig = plt.figure()
    fig.set_facecolor('black')

    ax = fig.add_subplot(111, projection='3d')
    ax.set_facecolor('black')

    particle_paths = [np.array([]) for _ in range(len(particle_list[0]))]
    
    ax.xaxis.line.set_color('black')
    ax.yaxis.line.set_color('black')
    ax.zaxis.line.set_color('black')
    ax.xaxis.pane.fill = ax.yaxis.pane.fill = ax.zaxis.pane.fill = 'black'
    ax.xaxis.pane.linecolor = ax.yaxis.pane.linecolor = ax.zaxis.pane.linecolor = 'black'

    animation = FuncAnimation(fig, update, frames=len(times_list), interval=50, repeat=False)

    # Save the animation as a GIF
    animation.save(animation_name, writer='pillow', fps=30)


def animate_particle_path_2D(times_list, particle_list, integrator_name, animation_name, ecc):
    """
    Animates the 2D path of particles over time.

    Parameters:
    - times_list (list): List of time values corresponding to each frame.
    - particle_list (list): List of particle positions for each frame.
    - integrator_name (str): Name of the integrator used.
    - ecc (float): Eccentricity value.

    Returns:
    - Animation: Displays the 2D animation.
    """
    max_x = max(particle[0] for frame in particle_list for particle in frame)
    min_x = min(particle[0] for frame in particle_list for particle in frame)
    max_y = max(particle[1] for frame in particle_list for particle in frame)
    min_y = min(particle[1] for frame in particle_list for particle in frame)

    def update(frame):
        ax.clear()
        ax.set_title(f'Time: {times_list[frame]} for {integrator_name} and ecc = {ecc}', color='white')

        for i, particles in enumerate(particle_list[frame]):
            x, y, _ = particles
            size = 200 if i == 0 else 100
            color = 'blue' if i == 0 else 'orange'
            ax.scatter(x, y, marker='o', s=size, color=color, label='Particle')

            if frame == 0:
                particle_paths[i] = np.array([[x, y]])
            else:
                particle_paths[i] = np.concatenate([particle_paths[i], [[x, y]]])

            ax.plot(
                particle_paths[i][:, 0], 
                particle_paths[i][:, 1],
                color=color,  # Use the assigned color here
                linestyle='--',
                alpha=0.5
            )

        ax.set_xlabel('X', color='white')
        ax.set_ylabel('Y', color='white')
        ax.legend()

        ax.set_xlim(min_x - 1, max_x + 1)
        ax.set_ylim(min_y - 1, max_y + 1)

        ax.xaxis.set_major_locator(plt.MaxNLocator(integer=True))
        ax.yaxis.set_major_locator(plt.MaxNLocator(integer=True))
        ax.set_facecolor('black')  # Set background color to black
        ax.grid(True, linestyle='-', linewidth=0.5, alpha=0.5, color='black')  # Set grid color to black

    fig, ax = plt.subplots()
    fig.set_facecolor('black')

    particle_paths = [np.array([]) for _ in range(len(particle_list[0]))]

    animation = FuncAnimation(fig, update, frames=len(times_list), interval=75, repeat=False)

    # Save the animation as a GIF
    animation.save(animation_name, writer='pillow', fps=30)


def evolve_and_plot_system(integrators, timestep, timesteps, masses, eccentricity, radii, num_particles, animate_particle_path, dynamics, animation_name, adaptive_timestep_function=None, fast_sim = False):
    """
    Evolves and plots the system over time.

    Args:
    - initial_conditions: Initial conditions for the system.
    - integrators: Dictionary of integrators and their names.
    - ecc: Eccentricity of the system.
    - adaptive_timestep_function: Function for adaptive time step calculation.
    - adaptive_timestep: Flag indicating whether to use adaptive time step.
    """
    if num_particles == 2:
        mass1 = masses[0]
        mass2 = masses[1]
        ecc = eccentricity
        rp = radii[0]

        a = rp / (1 - ecc)
        T_period = 2 * np.pi * (a ** 3 / ((mass1 + mass2))) ** 0.5
        total_time = 10 * T_period

        fig, axes = plt.subplots(1, len(integrators), figsize=(17, 5))

        if adaptive_timestep_function:
            fig.suptitle("2 Particle Simulation with Adaptive\n(Mass 1 = {}, Mass 2 = {}, Ecc = {}, r = {})".format(mass1, mass2, ecc, rp),
                        fontsize=14)
            fig.subplots_adjust(top=0.8, bottom=0.2)
        else:
            fig.suptitle("2 Particle Simulation\n(Mass 1 = {}, Mass 2 = {}, Ecc = {}, r = {})".format(mass1, mass2, ecc, rp),
                        fontsize=14)
            fig.subplots_adjust(top=0.8, bottom=0.2)

        for idx, (integrator_name, integrator) in enumerate(integrators.items()):
            # Example: Initial conditions for a circular orbit
            initial_conditions = ic_two_body(mass1, mass2, rp, ecc)
            particle_list, times_list = evolve_system_particles(initial_conditions, dynamics,
                                                                    integrator, total_time, timestep,
                                                                    adaptive_timestep_function, fast_sim)
            ax = axes[idx]
            evolve_and_plot_particles(particle_list, integrator_name, num_particles, ax=ax)

        plt.show()

        integrator_names = []
        times = []
        E = []
        E_err = []
        for idx, (integrator_name, integrator) in enumerate(integrators.items()):
            for ts in timesteps:
                # Example: Initial conditions for a circular orbit
                initial_conditions = ic_two_body(mass1, mass2, rp, ecc)
                selected_times, energies, energy_errors = evolve_system_energies(initial_conditions,
                                                                                    dynamics,
                                                                                    integrator, total_time, ts,
                                                                                    adaptive_timestep_function, fast_sim)
                times.append(selected_times)
                E.append(energies)
                E_err.append(energy_errors)
                integrator_names.append(integrator_name)

        evolve_and_plot_energies(times, E_err, timesteps, integrator_names, len(integrators), adaptive_timestep_function, "Energy Error")
        evolve_and_plot_energies(times, E, timesteps, integrator_names, len(integrators), adaptive_timestep_function, "Energy")

        times_list_subset = times_list[::10]
        particle_list_subset = particle_list[::10]

        animate_particle_path(times_list_subset, particle_list_subset, integrator_names[-1], animation_name, ecc)

    if num_particles == 3:

        mass1 = masses[0]
        mass2 = masses[1]
        mass3 = masses[2]

        Tevolve = 30

        total_time = Tevolve

        num_integrators = len(integrators)

        fig, axes = plt.subplots(1, len(integrators), figsize=(17, 5))

        if adaptive_timestep_function:
            fig.suptitle("3 Particle Simulation with Adaptive\n(Mass 1 = {}, Mass 2 = {}, Mass 3 = {})".format(
                mass1, mass2, mass3), fontsize=14)
            fig.subplots_adjust(top=0.8, bottom=0.2)
        else:
            fig.suptitle("3 Particle Simulation\n(Mass 1 = {}, Mass 2 = {}, Mass 3 = {})".format(
                mass1, mass2, mass3), fontsize=14)
            fig.subplots_adjust(top=0.8, bottom=0.2)

        for idx, (integrator_name, integrator) in enumerate(integrators.items()):
            initial_conditions = ic_three_body(mass1, mass2, mass3)
            particle_list, times_list = evolve_system_particles(initial_conditions, dynamics,
                                                                        integrator, total_time, timestep,
                                                                        adaptive_timestep_function, fast_sim)
            ax = axes[idx] if num_integrators > 1 else axes
            evolve_and_plot_particles(particle_list, integrator_name, num_particles, ax=ax)

        plt.show()

        integrator_names = []
        times = []
        E = []
        E_err = []
        for idx, (integrator_name, integrator) in enumerate(integrators.items()):
            for ts in timesteps:
                initial_conditions = ic_three_body(mass1, mass2, mass3)
                selected_times, energies, energy_errors = evolve_system_energies(initial_conditions,
                                                                                    dynamics,
                                                                                    integrator, total_time, ts,
                                                                                    adaptive_timestep_function, fast_sim)
                times.append(selected_times)
                E.append(energies)
                E_err.append(energy_errors)
                integrator_names.append(integrator_name)

        evolve_and_plot_energies(times, E_err, timesteps, integrator_names, len(integrators), adaptive_timestep_function, "Energy Error")
        evolve_and_plot_energies(times, E, timesteps, integrator_names, len(integrators), adaptive_timestep_function, "Energy")

        times_list_subset = times_list[::10]
        particle_list_subset = particle_list[::10]

        animate_particle_path(times_list_subset, particle_list_subset, integrator_names[-1], animation_name, 0)

"""
==============================================================
Initial conditions utilities , (:mod:`fireworks.ic`)
==============================================================

This module contains functions and utilities to generate
initial conditions for the Nbody simulations.
The basic idea is that each function/class should returns
an instance of the class :class:`~fireworks.particles.Particles`

"""

import numpy as np
from .particles import Particles

def ic_random_normal(N: int, mass: float=1) -> Particles:
    """
    Generate random initial condition drawing from a normal distribution
    (centred in 0 and with dispersion 1) for the position and velocity.
    The mass is instead the same for all the particles.

    :param N: number of particles to generate
    :param mass: mass of the particles
    :return: An instance of the class :class:`~fireworks.particles.Particles` containing the generated particles
    """

    pos  = np.random.normal(size=3*N).reshape(N,3) # Generate 3xN 1D array and then reshape as a Nx3 array
    vel  = np.random.normal(size=3*N).reshape(N,3) # Generate 3xN 1D array and then reshape as a Nx3 array
    mass = np.ones(N)*mass

    return Particles(position=pos, velocity=vel, mass=mass)

def ic_random_uniform(N: int, min_max_pos=[None,None], min_max_vel=[None,None], min_max_mass=[None,None]) -> Particles:
    """
    Generate random initial condition drawing from a uniform distribution
    (centred in 0 and with dispersion 1) for the position and velocity.
    The mass is instead the same for all the particles.

    :param N: number of particles to generate
    :param mass: mass of the particles
    :return: An instance of the class :class:`~fireworks.particles.Particles` containing the generated particles
    """

    pos  = np.random.uniform(low=min_max_pos[0], high=min_max_pos[1], size=3*N).reshape(N,3) # Generate 3xN 1D array and then reshape as a Nx3 array
    vel  = np.random.uniform(low=min_max_vel[0], high=min_max_vel[1],size=3*N).reshape(N,3) # Generate 3xN 1D array and then reshape as a Nx3 array
    mass = np.random.uniform(low=min_max_mass[0], high=min_max_mass[1],size=N)

    return Particles(position=pos, velocity=vel, mass=mass)


def ic_two_body(mass1: float, mass2: float, rp: float, e: float):
    """
    Create initial conditions for a two-body system.
    By default the two bodies will placed along the x-axis at the
    closest distance rp.
    Depending on the input eccentricity the two bodies can be in a
    circular (e<1), parabolic (e=1) or hyperbolic orbit (e>1).

    :param mass1:  mass of the first body [nbody units]
    :param mass2:  mass of the second body [nbody units]
    :param rp: closest orbital distance [nbody units]
    :param e: eccentricity
    :return: An instance of the class :class:`~fireworks.particles.Particles` containing the generated particles
    """

    Mtot=mass1+mass2

    if e==1.:
        vrel=np.sqrt(2*Mtot/rp)
    else:
        a=rp/(1-e)
        vrel=np.sqrt(Mtot*(2./rp-1./a))

    # To take the component velocities
    # V1 = Vcom - m2/M Vrel
    # V2 = Vcom + m1/M Vrel
    # we assume Vcom=0.
    v1 = -mass2/Mtot * vrel
    v2 = mass1/Mtot * vrel

    pos  = np.array([[0.,0.,0.],[rp,0.,0.]])
    vel  = np.array([[0.,v1,0.],[0.,v2,0.]])
    mass = np.array([mass1,mass2])

    return Particles(position=pos, velocity=vel, mass=mass)

def ic_three_body(mass1: float, mass2: float, mass3: float):
    """
    Create initial conditions for a three-body system.
    By default, the bodies will be placed along the x-axis at the closest distances rp1_2, rp2_3, and rp3_1.
    Depending on the input eccentricities, the bodies can be in circular (e_i < 1), parabolic (e_i = 1), or hyperbolic orbits (e_i > 1).

    :param mass1:  mass of the first body [nbody units]
    :param mass2:  mass of the second body [nbody units]
    :param mass3:  mass of the third body [nbody units]
    :param rp1_2: closest orbital distance between bodies 1 and 2 [nbody units]
    :param rp2_3: closest orbital distance between bodies 2 and 3 [nbody units]
    :param rp3_1: closest orbital distance between bodies 3 and 1 [nbody units]
    :param e1_2: eccentricity for the orbit between bodies 1 and 2
    :param e2_3: eccentricity for the orbit between bodies 2 and 3
    :param e3_1: eccentricity for the orbit between bodies 3 and 1
    :return: An instance of the class :class:`~fireworks.particles.Particles` containing the generated particles
    """

    # Set initial positions and velocities
    pos = np.array([[1, 3, 0], [-2, -1, 0], [1, -1, 0]])
    vel = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]])
    mass = np.array([mass1, mass2, mass3])

    return Particles(position=pos, velocity=vel, mass=mass)

